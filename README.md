# Task
Make 4 order authentucation system with 4 different guard/provider using 2 different table. 

01. admin table
02. customer table

01. System Admin (admin table)
02. Admin (admin table)
03. Manager (admin table)
04. Customer (customer table)

They will login with same api and System Admin will be default created with seeder

01. System Admin can create Admin
02. Admin can create manager
03. Customer can register himself & manager can see them 

### So total Auth system is 
01. Login
02. Admin creation
03. Manager creation
04. Customer registration and login with same API which other users are using with login page
05. Manager can see Cutomer list


# Soultion

### System Admin, Admin, Manager 
> Login : https://localhost:8000/admin/login

> Dashboard : https://localhost:8000/admin

> System Admin email = super@admin.com and password = 123456

### Customer
> Login : http://127.0.0.1:8000/login

> Dashboard : http://127.0.0.1:8000/home

#### Total auth system is created!


# Task 2

Make an media manager to store & use media (image/video/audio/documents) files as a cdn server
### Solution
> Url : http://127.0.0.1:8000/media

> Feature : Add Folder, File Upload, File (move, delete, rename).

> Any document can be accessed by using the link from anywhere.


Make an api with which user can access images as dynamic resolution without making new files in server

### Solution
> Parameters : img, width, height. img is the image name which is mandatory.

> Api url : http://127.0.0.1:8000/api/image

> Example : http://127.0.0.1:8000/api/image?img=certificate.png&height=500


Example : https://stage-api.equidesk.com/api/auth/preview/upload/images?w=500&img=5bc5ed6311447.jpg


# Setup & Run
Create database & setup `.env` file accordingly
- Run ` composer install `
- Run ` php artisan key:generate `
- Run ` php artisan migrate `
- Run ` php artisan db:seed `
- Run ` php artisan serve `

Goto ` http://127.0.0.1:8000 ` to check
