<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Admin;
use App\User;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $admins = Admin::where('role','Admin')->get();
        $managers = Admin::where('role','Manager')->get();
        $customers = User::all();
        // var_dump($admins);
        // var_dump($customers);
        // echo '<pre>' . var_export($admins, true) . '</pre>';
        return view('admin')->with('admins', $admins)->with('customers',$customers)->with('managers',$managers);
    }
    
    /**
     * Create manager view
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function createManagerView()
    {
        if(Auth::user()->role !== 'Admin'){
            return view('admin')->with('message' , "Only Admin Can Add Manager!");
        }
        
        return view('auth.create-manager');
    }

    /**
     * Create manager Controller
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function createManager(Request $request)
    {
        if(Auth::user()->role !== 'Admin'){
            return view('admin');
        }

        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email|unique:admins',
            'password' => 'required|min:6'
        ]);

        Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'role' => 'Manager',
        ]);

        $managers = Admin::where('role','Manager')->get();

        return view('admin')->with('message','Manager creation successful!')->with('managers',$managers);
    }

     /**
     * Create admin view
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function createAdminView()
    {
        if(Auth::user()->role !== 'System Admin'){
            return view('admin')->with('message' , "Only System Admin Can Add Admin!");
        }
        return view('auth.create-admin');
    }

    /**
     * Create admin Controller
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function createAdmin(Request $request)
    {
        if(Auth::user()->role !== 'System Admin'){
            return view('admin');
        }

        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email|unique:admins',
            'password' => 'required|min:6'
        ]);

        Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'role' => 'Admin',
        ]);

        $admins = Admin::where('role','Admin')->get();

        return view('admin')->with('message','Admin creation successful!')->with('admins', $admins);
    }
}
