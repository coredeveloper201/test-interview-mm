<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use Intervention\Image\Facades\Image;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getImage(Request $request)
    {
        // assuming the parameters are in correct value
        // TODO : validate params

        $img_name = trim($request['img']);
        $width = $request['width'];
        $height = $request['height'];

        $filepath = 'storage/'.$img_name;
        // var_dump($img_name, file_exists($filepath), $filepath);

        if(file_exists($filepath)){
            $image = Image::make($filepath);
            if($width && $height && intval($width) > 0 && intval($height) > 0){
                $width = intval($width);
                $height = intval($height);
                $image->resize($width, $height);
                return $image->response();
            }
            else if($width && intval($width) > 0){
                $width = intval($width);
                // maintaining aspect ratio
                $image->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                return $image->response();
            }
            else if($height && intval($height) > 0){
                $height = intval($height);
                // maintaining aspect ratio
                $image->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
                return $image->response();
            }
            else if($width || $height){
                $returnData = array(
                    'status' => 'error',
                    'message' => 'Invalid dimension!'
                );
                return Response::json($returnData, 401);
            }
            else{
                return $image->response();
            }
        }

        $returnData = array(
            'status' => 'error',
            'message' => 'Image doesn\'t exists!'
        );
        return Response::json($returnData, 401);
    }


}
