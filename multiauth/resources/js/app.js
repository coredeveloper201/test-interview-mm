
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// require('../assets/vendor/MediaManager/js/manager');
require('../assets/talvbansal/media-manager/js/media-manager');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        showMediaManager: false,
        selectedEventName: 'editor'
    },
    created: function(){
        window.eventHub.$on('media-manager-notification', function (message, type, time) {
            // Your custom notifiction call here...
            console.log(message);
        });
    },
    mounted(){
        window.eventHub.$on('media-manager-selected-editor', (file) => {
            // Do something with the file info...
            console.log(file.name);
            console.log(file.mimeType);
            console.log(file.relativePath);
            console.log(file.webPath);

            // Hide the Media Manager...
            this.showMediaManager = false;
        });
    }
});
